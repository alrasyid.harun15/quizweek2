<?php 
	
	
	function find_operator(string $string, string $operator):int{

		for($i=0; $i<strlen($string); $i++){
			
			if($string[$i]==$operator){
				return $i;
			}
		}

		return -1;
	}


	function hitung(string $param):string{

		if(strlen($param)>=3 ){

			

			if(find_operator($param, '+')>-1){

				$idx = find_operator($param, '+');
				$x = (int)substr($param, 0,$idx+1);
				$y = (int)substr($param,$idx+1,strlen($param)-$idx);

				return $x + $y  ;
				

			}else if(find_operator($param, '-')>-1){

				$idx = find_operator($param, '-');
				$x = (int)substr($param, 0,$idx+1);
				$y = (int)substr($param,$idx+1,strlen($param)-$idx);

				return $x - $y  ;

			}else if(find_operator($param, '*')>-1){

				$idx = find_operator($param, '*');
				$x = (int)substr($param, 0,$idx+1);
				$y = (int)substr($param,$idx+1,strlen($param)-$idx);

				return $x * $y  ;

			}else if(find_operator($param, '/')>-1){

				$idx = find_operator($param, '/');
				$x = (int)substr($param, 0,$idx+1);
				$y = (int)substr($param,$idx+1,strlen($param)-$idx);

				return $x / $y  ;

			}else if(find_operator($param, '%')>-1){

				$idx = find_operator($param, '%');
				$x = (int)substr($param, 0,$idx+1);
				$y = (int)substr($param,$idx+1,strlen($param)-$idx);

				return $x & $y  ;

			}else{
				return 'parameter tidak sesuai';	
			}		

			
		}else{
			return 'parameter tidak sesuai!';
		}

	}

	echo "Hasil perhitungan adalah : " . hitung('100+200') . " <br>";
	echo "Hasil perhitungan adalah : " . hitung('100-20') . " <br>";
	echo "Hasil perhitungan adalah : " . hitung('200*2') . " <br>";
	echo "Hasil perhitungan adalah : " . hitung('100/2') . " <br>";
	echo "Hasil perhitungan adalah : " . hitung('5%2') . " <br>";

 ?>
