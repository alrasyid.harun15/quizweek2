#create table customers

CREATE TABLE CUSTOMERS (
    id 			INT NOT NULL AUTO_INCREMENT,
    name 		VARCHAR(255),
    email 		VARCHAR(255),
   	password 	VARCHAR(255),
   	PRIMARY KEY (id)
);


#create table orders

CREATE TABLE ORDERS (
    id 				INT NOT NULL AUTO_INCREMENT PRIMARY KEY,    
   	amount 			INT,
   	customer_id 	INT,
   	CONSTRAINT fk_customer_id 
	   	FOREIGN KEY (customer_id) 
	   		REFERENCES customers (id)
);